import 'dart:ffi';
import 'dart:io';

class PayerState {
  int _playerSate = 1; // 1 - by deafult
  int _turnNumber = 0;
  void nextTurn() {
    ++_turnNumber;
    if (_playerSate == 1)
      _playerSate = 2;
    else if (_playerSate == 2) _playerSate = 1;
  }

  int getTurnNuber() {
    return _turnNumber;
  }

  String getActivePlayer() {
    if (_playerSate == 1) return "X";
    if (_playerSate == 2) return "O";
    return "";
  }
}

class GameField {
  List<int> field = [-1, -1, -1, -1, -1, -1, -1, -1, -1];
  String winner = "";
  bool isWinCombitaionDone(List<int>? list) {
    if (list!.length != 3) return false;
    int prevValue = field[list[0]];
    if (prevValue == -1) return false;
    int summ = 0;

    bool ok = true;
    list.forEach((element) {
      summ += field[element];
      if (prevValue != field[element]) {
        ok = false;
      }
    });
    if (!ok) return false;
    if (summ == 3) {
      winner = "X";
      return true;
    } else if (summ == 0) {
      winner = "0";
      return true;
    }
    return false;
  }

  bool isGameFinish() {
    const List<List<int>> winCombi = [
      [0, 4, 8],
      [2, 4, 6],
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8]
    ];

    for (var element in winCombi) {
      if (isWinCombitaionDone(element)) return true;
    }

    return false;
  }

  String _getFieldSymbol(int index) {
    if (index >= 0 && index < field.length) {
      switch (field[index]) {
        case -1:
          return " ";
        case 0:
          return "O";
        case 1:
          return "X";
      }
    }
    return "";
  }

  String whoIsWin() {
    return winner;
  }

  bool validate(int x, int y) {
    if ((x >= 1 && x <= 3) && (y >= 1 && y <= 3)) {
      return true;
    }
    return false;
  }

  bool setValueToCell({int x = 0, int y = 0, String value = ""}) {
    if (!validate(x, y)) return false;
    int cellValue = value.contains("X") ? 1 : 0;
    int listPosition = (x - 1) + (3) * (y - 1);
    print('listPosition $listPosition');
    if (field[listPosition] != -1) return false;
    field[listPosition] = cellValue;
    return true;
  }

  void printField() {
    print("  1   2   3");
    print("1:" +
        _getFieldSymbol(0) +
        " | " +
        _getFieldSymbol(1) +
        " | " +
        _getFieldSymbol(2));
    print("-----------");
    print("2:" +
        _getFieldSymbol(3) +
        " | " +
        _getFieldSymbol(4) +
        " | " +
        _getFieldSymbol(5));
    print("-----------");
    print("3:" +
        _getFieldSymbol(6) +
        " | " +
        _getFieldSymbol(7) +
        " | " +
        _getFieldSymbol(8));
    print("");
  }
}

void main(List<String> arg) {
  GameField field = GameField();
  PayerState ps = PayerState();
  while (!field.isGameFinish()) {
    field.printField();
    print(
        "Turn ${ps.getTurnNuber()} Playear ${ps.getActivePlayer()}  enter poxition x,y");
    var input = stdin.readLineSync();
    var list = input?.split(',');
    if (list?.length != 2) {
      print("broken input... plese use constructionlike this: '3,2'");
      continue;
    }
    if (list![0].isEmpty || list![1].isEmpty) {
      print("broken input... plese use constructionlike this: '3,2'");

      continue;
    }

    int x = int.parse(list![0]);
    int y = int.parse(list![1]);
    if (!field.validate(x, y)) {
      print("broken input... plese use constructionlike this: '3,2'");

      continue;
    }
    bool ok = field.setValueToCell(x: x, y: y, value: ps.getActivePlayer());
    if (!ok) {
      print("broken input... may be cell is buisy..");
      continue;
    }
    ps.nextTurn();
  }

  print("WINNER is ${field.whoIsWin()} !!!!");
}
