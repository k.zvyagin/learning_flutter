import 'dart:convert';
import 'dart:io';

double sub(double a, double b) {
  return a - b;
}

double add(double a, double b) {
  return a + b;
}

double div(double a, double b) {
  return a / b;
}

double mul(double a, double b) {
  return a * b;
}

Map<String, Function> operators1 = {"+": add, "-": sub, "/": div, "*": mul};

bool simpleDigint(String str) {
  if (str.contains("*") ||
      str.contains("-") ||
      str.contains("+") ||
      str.contains("*/")) return false;
  return true;
}

List<String> splitByOPerator(String str) {
  List<String> list = <String>[];
  if (str.contains("*")) {
    list = str.split("*");
    list.add("*");
  } else if (str.contains("/")) {
    list = str.split("/");
    list.add("/");
  } else if (str.contains("+")) {
    list = str.split("+");
    list.add("+");
  } else if (str.contains("-")) {
    list = str.split("-");
    list.add("-");
  }
  return list;
}

double calculate(String a, String b, String p) {
  bool aIsSimple = simpleDigint(a);
  bool bIsSimple = simpleDigint(b);
  if (b.isEmpty) {
    var list = splitByOPerator(a);
    if (list.length == 3) return calculate(list[0], list[1], list[2]);
  } else if (!a.isEmpty && !b.isEmpty && aIsSimple && bIsSimple && !p.isEmpty) {
    var da = double.parse(a);
    var db = double.parse(b);
    var f = operators1[p];
    return f!(da, db);
  } else if (!aIsSimple || !bIsSimple) {
    print("It's too complicated expression");
    return 0;
    var longExpression = a;
    if (!bIsSimple) longExpression = b;
    var list = splitByOPerator(longExpression);
    double result1 = 0;
    if (list.length == 3) result1 = calculate(list[0], list[1], list[2]);
  }

  return 0;
}

main() {
  while (true) {
    print("Enter expression. If you want to exit, write 'exit'");
    var line = stdin.readLineSync(encoding: utf8).toString();
    if (line.contains("exit") == true) return;
    print("readed line");
    print(line);
    var result = calculate(line, "", "");
    print("resut: $result");
  }
}
