#!/bin/bash
# This is a bash script to compile aurora flutter app
FLUTTER_AURORA=/home/user/.local/opt/flutter-sdk/bin/flutter
KEY_PATH=/home/user/dev_keys/regular_key.pem
CERT_PATH=/home/user/dev_keys/regular_cert.pem

if [ -z "${PSDK_DIR}" ]
then
	echo "Error ! PSDK_DIR is empty. Please set this vatiable in .bashrc and rerun script"
	exit
fi
echo "Setting PSDK configuration..."
$PSDK_DIR/sdk-chroot sb2-config -d AuroraOS-4.0.2.249-armv7hl
echo "Starting flutter pub get command to get application depencies"
$FLUTTER_AURORA pub get
echo "Evaluate building flutter application..."
$FLUTTER_AURORA build aurora --release
echo "Make rpm signature ..."

"$PSDK_DIR"/sdk-chroot rpmsign-external sign     --key $KEY_PATH    --cert $CERT_PATH    ./build/aurora/arm/release/RPMS/*.rpm
echo "MAYBE DONE"



