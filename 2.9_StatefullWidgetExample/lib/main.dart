import 'package:flutter/material.dart';
import 'dart:async';

void main() => runApp(MyFirstApp());

class MyFirstApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyFirstAppState();
  }
}

class _MyFirstAppState extends State<MyFirstApp> {
  bool _loading = false;
  double _progressValue = 0;
  @override
  void initState() {
    _loading = false;
    _progressValue = 0.0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.indigo,
        appBar: AppBar(
          title: Text("My first APP"),
          centerTitle: false,
        ),
        body: Center(
          child: _loading
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    LinearProgressIndicator(
                      value: _progressValue,
                    ),
                    Text('${(_progressValue * 100).round()} %'),
                    Text("Download in progress")
                  ],
                )
              : Text("Press button to download"),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              if (_progressValue >= 1.0) {
                _progressValue = 0;
              }
              // setState calls build method
              _loading = !_loading;
              _updateProgress();
            });
          },
          child: Icon(Icons.cloud_download),
        ),
      ),
    );
  }

  void _updateProgress() {
    const oneSec = const Duration(milliseconds: 10);
    Timer.periodic(oneSec, (Timer t) {
      setState(() {
        _progressValue = _progressValue + 0.01;
        if (_progressValue >= 1.0) {
          _loading = false;
          t.cancel();
          _progressValue = 1.0;
        }
      });
    });
  }
}
